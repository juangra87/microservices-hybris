package com.demo.oracle_controller;

import com.demo.database.CustomerRepository;
import com.demo.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@RestController
public class OracleDBAccessController {

    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping("/")
    public String wellcome() {
        return "Wellcome on board!";
    }

    @GetMapping("/getusers")
    public Iterable<Customer> getUser(){
        Iterable<Customer> customers = customerRepository.findAll();
        return customers;
    }

    @GetMapping("/count")
    public Long getUsersCount(){
        return customerRepository.count();
    }

    @PostMapping(name = "/createuser")
    public String createUser(@RequestBody Customer customer) {
        try {
            customerRepository.save(customer);
        } catch (Exception e) {
            return "K.O.";
        }
        return "200";
    }

    @DeleteMapping(name = "/removeuser")
    public String removeUser(@RequestBody Long pk) {
        try {
            customerRepository.deleteById(pk);
        } catch (Exception e) {
            return "K.O.";
        }
        return "200";
    }
}
